/*
 * Задача 6. Дан ориентированный граф. Определите, какое минимальное
 * количество ребер необходимо добавить, чтобы граф стал сильносвязным.
 * В графе возможны петли. Время - O(V+E). Память - O(V)
 */
#include <algorithm>
#include <iostream>
#include <list>
#include <stack>
#include <vector>

using std::list;
using std::stack;
using std::vector;

struct IGraph {
  virtual ~IGraph() {}

  virtual void AddEdge(int from, int to) = 0;

  virtual int VerticesCount() const = 0;

  virtual void GetNextVertices(int vertex,
                               std::vector<int> &vertices) const = 0;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int> &vertices) const = 0;
};

struct ListGraph : public IGraph {
 private:
  std::vector<std::list<int> > adjacency_list;
  std::vector<std::list<int> > reverse_adjacency_list;

 public:
  ListGraph(int n) : adjacency_list(n), reverse_adjacency_list(n) {}
  ~ListGraph() = default;
  ListGraph(const IGraph *graph);
  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int> &vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int> &vertices) const override;
};

void straight_dfs(const IGraph &graph, int start, vector<bool> &visited,
                  vector<int> &topsort);

void reverse_dfs(const IGraph &graph, int start, vector<bool> &visited,
                 vector<int> &colors, int color);

int condensation_build(const IGraph &graph, vector<int> &colors);
int min_strong_connect_edges(const IGraph &graph);

int main() {
  int vertices, edges;
  std::cin >> vertices >> edges;
  ListGraph graph(vertices);
  for (int i = 0; i < edges; i++) {
    int end, start;
    std::cin >> start >> end;
    graph.AddEdge(start - 1, end - 1);
  }
  std::cout << min_strong_connect_edges(graph) << std::endl;

  return 0;
}

int min_strong_connect_edges(const IGraph &graph) {
  int verticesCount = graph.VerticesCount();
  vector<int> cond(verticesCount);
  int connected_components =
      condensation_build(graph, cond);  // Cтроим конденсацию
  int sources_count = 0;
  int stock_count = 0;
  vector<char> is_source(connected_components, 1);
  vector<char> is_stock(connected_components, 1);
  for (int vertex = 0; vertex < verticesCount; vertex++) {  // Считаем кол-во истоков и стоков
    vector<int> nextVertices;
    graph.GetNextVertices(vertex, nextVertices);
    for (auto neigh : nextVertices) {
      if (cond[neigh] != cond[vertex]) {
        is_source[cond[neigh]] = false;
        is_stock[cond[vertex]] = false;
      }
    }
  }
  for (auto ver : is_source) {
    sources_count += ver;
  }
  for (auto ver : is_stock) {
    stock_count += ver;
  }
  if (connected_components == 1) {
    return 0;
  }
  return std::max(sources_count, stock_count);
}
int condensation_build(const IGraph &graph, vector<int> &colors) {
  int verticesCount = graph.VerticesCount();
  vector<int> topsort;
  vector<bool> visited(verticesCount);
  for (int vertex = 0; vertex < verticesCount; vertex++)  // Cтроим topsort
    if (!visited[vertex]) straight_dfs(graph, vertex, visited, topsort);

  visited.assign(verticesCount, false);
  std::reverse(topsort.begin(), topsort.end());
  int cur_color = 0;
  for (auto vertex : topsort) {
    if (!visited[vertex]) {
      reverse_dfs(graph, vertex, visited, colors, cur_color);
      cur_color++;
    }
  }
  return cur_color;
}
void straight_dfs(const IGraph &graph, int start, vector<bool> &visited,
                  vector<int> &topsort) {
  std::stack<std::pair<int, int> >
      stack;  // Состояние это вершина + номер с которой продолжать
  stack.push({start, 0});
  visited[start] = true;
  while (!stack.empty()) {
    std::pair<int, int> state = stack.top();
    stack.pop();
    vector<int> nextVertices;
    graph.GetNextVertices(state.first, nextVertices);
    bool exit_flag = false;
    for (int it = state.second; it < nextVertices.size(); it++) {
      if (!visited[nextVertices[it]]) {
        visited[nextVertices[it]] = true;
        stack.push({state.first, it + 1});
        stack.push({nextVertices[it], 0});
        exit_flag = true;
        break;
      }
    }
    if (!exit_flag) topsort.push_back(state.first);
  }
}
void reverse_dfs(const IGraph &graph, int start, vector<bool> &visited,
                 vector<int> &colors, int color) {
  std::stack<std::pair<int, int> >
      stack;  // Состояние это вершина + номер с которой продолжать
  stack.push({start, 0});
  visited[start] = true;
  colors[start] = color;
  while (!stack.empty()) {
    std::pair<int, int> state = stack.top();
    stack.pop();
    vector<int> prevVertices;
    graph.GetPrevVertices(state.first,
                          prevVertices);  // Идем по обратным ребрам
    for (int it = state.second; it < prevVertices.size(); it++) {
      if (!visited[prevVertices[it]]) {
        visited[prevVertices[it]] = true;
        colors[prevVertices[it]] = color;
        stack.push({state.first, it + 1});
        stack.push({prevVertices[it], 0});
        break;
      }
    }
  }
}

void ListGraph::AddEdge(int from, int to) {
  ListGraph::adjacency_list[from].push_front(to);
  ListGraph::reverse_adjacency_list[to].push_front(from);
}

int ListGraph::VerticesCount() const {
  return ListGraph::adjacency_list.size();
}

void ListGraph::GetNextVertices(int vertex, vector<int> &vertices) const {
  for (auto it : ListGraph::adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

void ListGraph::GetPrevVertices(int vertex, vector<int> &vertices) const {
  for (auto it : ListGraph::reverse_adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

ListGraph::ListGraph(const IGraph *graph)
    : adjacency_list(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < adjacency_list.size(); i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    adjacency_list[i].insert(adjacency_list[i].end(), temp.begin(), temp.end());
  }
}
